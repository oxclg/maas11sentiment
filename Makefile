TOKENIZE=tokenize-anything.sh

all: unlabelled_train labelled_train labelled_dev labelled_test

file_clean:
	for i in train/* test/*; do cat $$i | scripts/clean_dataset > $$i.clean; done

dev_split: file_clean
	for i in train/neg.clean train/pos.clean; do python scripts/devsplit.py $$i; done

unlabelled_train: file_clean
	cat train/unsup.clean | cut -d ' ' -f 2- | $(TOKENIZE) > unlabelled_train.docs

labelled_train: dev_split
	cat train/pos.clean.train train/neg.clean.train | cut -d ' ' -f 1 > labelled_train.labels
	cat train/pos.clean.train train/neg.clean.train | cut -d ' ' -f 2- | $(TOKENIZE) > labelled_train.docs

labelled_dev: dev_split
	cat train/pos.clean.dev train/neg.clean.dev | cut -d ' ' -f 1 > labelled_dev.labels
	cat train/pos.clean.dev train/neg.clean.dev | cut -d ' ' -f 2- | $(TOKENIZE) > labelled_dev.docs

labelled_test: file_clean
	cat test/pos.clean test/neg.clean | cut -d ' ' -f 1 > labelled_test.labels
	cat test/pos.clean test/neg.clean | cut -d ' ' -f 2- | $(TOKENIZE) > labelled_test.docs

clean:
	rm train/*.clean test/*.clean
	rm train/*.dev train/*.train
	rm labelled_dev.docs labelled_dev.labels
	rm labelled_test.docs labelled_test.labels
	rm labelled_train.docs labelled_train.labels
	rm unlabelled_train.docs