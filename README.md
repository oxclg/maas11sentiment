# Outline

This is a cleaned and reformatted version of the [Maas et al. (2011) IMDb sentiment dataset](http://ai.stanford.edu/~amaas/data/sentiment/). Aside from the change in format (single documents for testing and training, instead of folders of documents), this dataset includes a new training/development set split, obtained from the original training dataset.

When using this dataset, please cite:

> Andrew L. Maas, Raymond E. Daly, Peter T. Pham, Dan Huang, Andrew Y. Ng, and Christopher Potts. (2011). [Learning Word Vectors for Sentiment Analysis](http://ai.stanford.edu/~amaas/papers/wvSent_acl2011.pdf). *The 49th Annual Meeting of the Association for Computational Linguistics (ACL 2011)*.

# Construction

The labelled training set has been randomly shuffled using `sort -R` and split into a new training set (90% of the original training set) and a development set (the remaining 10% of the original dataset). Both the new training and development sets are balanced for sentiment (50% positive labels, and 50% negative).

Each document has been cleaned using a custom html stripping script, and tokenised using `tokenize-anything.sh` from [cdec](https://github.com/redpony/cdec).

# Files and Formats
This project contains the following files:

* `./unlabelled_dev.docs` contains the documents from Andrew Maas' original unsupervised training set, one document per line.
* `./labelled_train.labels` and `./labelled_train.docs` contain the aligned labels and documents (one per line) from the new training set obtained from 90% of Andrew Maas' original training set, such that the `k`th line of `./labelled_train.labels` is the sentiment label for the document on the `k`th line of `./labelled_train.docs`.
* `./labelled_dev.labels` and `./labelled_dev.docs` contain the aligned labels and documents (one per line) from the new training set obtained from  the 10% of Andrew Maas' original training set not used in the new training set, such that the `k`th line of `./labelled_dev.labels` is the sentiment label for the document on the `k`th line of `./labelled_dev.docs`.
* `./labelled_test.labels` and `./labelled_test.docs` contain the aligned labels and documents (one per line) from Andrew Maas' original test set, such that the `k`th line of `./labelled_test.labels` is the sentiment label for the document on the `k`th line of `./labelled_test.docs`.
* `./train/` contains the documents (`./train/neg`, `./train/pos`, `./train/unsup`) from Andrew Maas' original training dataset in single files.  
Each file has one document per line, with its review score (a score from 1-10), in the following format:  
`REVIEW_SCORE DOCUMENT_WORD_1 DOCUMENT_WORD_2 ... DOCUMENT_WORD_k`
* `./test/` contains the documents (`./test/neg`, `./test/pos`) from Andrew Maas' original test dataset in single files.  
Each file has one document per line, with its review score (a score from 1-10), in the following format:  
`REVIEW_SCORE DOCUMENT_WORD_1 DOCUMENT_WORD_2 ... DOCUMENT_WORD_k`
* `./README.md` is this file, outlining the origin and construction of the reformatted dataset, and the file formats therein.
* `./Makefile` allows the whole dataset to be rebuilt with `make TOKENIZE=/path/to/tokenize-anything.sh`.
* `./scripts/html_strip` is a custom HTML stripping script which was adapted from a [Stack Overflow solution](http://stackoverflow.com/questions/753052/strip-html-from-strings-in-python).

# Further details

This reformatted dataset was produced by [Ed Grefenstette](http://www.egrefen.com), based on the version of [Andrew Maas' dataset](http://ai.stanford.edu/~amaas/data/sentiment/) obtained on Nov 13 2013, and was last updated on Dec 3 2013.